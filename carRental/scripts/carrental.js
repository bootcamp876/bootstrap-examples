"use strict";

window.onload = init;

function init() {
    const estBtn = document.getElementById("estBtn");

    estBtn.onclick = onEstBtnClicked;
}

function onEstBtnClicked() {
    const carRental = document.getElementById("carRental");
    const optionsRental = document.getElementById("optionsRental");
    const under25Surcharge = document.getElementById("under25Surcharge");
    const totalDue = document.getElementById("totalDue");
    const numberDays = document.getElementById("numberDays");

    let optionAmt = 0;
    let under25Amt = 29.99 * .3 * Number(numberDays.value);
    let carRentalAmt = Number(numberDays.value) * 29.99;
    carRental.innerHTML = `Car rental: ${carRentalAmt}`;

    let tollTag = document.getElementById("tollTag").checked;
    let gpsRental = document.getElementById("gpsRental").checked;
    let roadSideAssist = document.getElementById("roadSideAssist").checked;
    if (tollTag) {
        optionAmt += (3.95 * Number(numberDays.value));
    }
    if (gpsRental) {
        optionAmt += (2.95 * Number(numberDays.value));
    }
    if (roadSideAssist) {
        optionAmt += (2.95 * Number(numberDays.value));
    }
    optionsRental.innerHTML = `Options: ${optionAmt}`;

    if (document.getElementById("under25").checked) {
        under25Surcharge.innerHTML = `Under 25 surcharge ${under25Amt.toFixed(2)}`;
    }
    else {
        under25Surcharge.innerHTML = "";
    }
    let totalDueAmt = carRentalAmt + optionAmt + under25Amt;
    totalDue.innerHTML = `Total Due ${totalDueAmt.toFixed(2)}`;
}







/*
    let carRental = "29.99";
    let surcharge = ".30";

    <fieldset>
        <legend>ACME Car Rental</legend>
        <legend>Pickup Date:</legend>
        <input type="date" id="pickUpDate" pattern="^[0-9]" required />
    </fieldset>

    <fieldset>
        <legend>Pickup Date:</legend>
        <input type="number" id="numOfDays" pattern="^[0-9]" required />
    </fieldset>
  

    <fieldset>
        <legend>Options</legend>
        <input type="checkbox" value=""> Electronic Toll Tag ($3.95/day)>
        <input type="checkbox" value=""> GPS ($2.95/day)>
        <input type="checkbox" value=""> Roadside Assistance ($2.95/day)>
    </fieldset>

    <fieldset>
        <legend>Under 25</legend>
        <input name="below25" type="radio" value="No"> No<br>
        <input name="below25" type="radio" value="Yes"> Yes<br>
    </fieldset>

    <fieldset>
        <legend>Estimate Total Cost</legend><br>
        <label for="carRental">Car Rental:</label>
        <label for="Options">Options:</label><br>
        <label for="surcharge">Under 25 surcharge:</label>
    </fieldset>

    <fieldset>
        <label for="totalDue">Total Due:</label>

    </fieldset>
*/
