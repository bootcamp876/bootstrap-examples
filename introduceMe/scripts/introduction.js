"use strict";

function processInput() {
  // get the input fields
  //alert("started");
  const name = document.getElementById("nameInput");
  const hobby = document.getElementById("hobbyInput");
  const favoriteColor = document.getElementById("favoriteColor");
  const maxCoffee = document.getElementById("maxCoffee");
  const currentCoffee = document.getElementById("currentCoffee");

  // get the values
  let userName = name.value;
  let userHobby = hobby.value;
  let userFavoriteColor = favoriteColor.value;
  let userMaxCoffee = maxCoffee.value;
  let userCurrentCoffee = currentCoffee.value;
  
  // set values

  userName = "Toni";
  userHobby = "Family History Research";
  userFavoriteColor = "Purple";
  userMaxCoffee = 2;
  userCurrentCoffee = 1;


  // create the message
  let message = `Hi ${userName}, you drank ${userCurrentCoffee} of ${userMaxCoffee} cups of coffee today. 
                Your favorite color is ${userFavoriteColor} and your hobby is ${userHobby}`;

  // set the message as innerHTML of the paragraph
  let paragraph = document.getElementById("hiParagraph");
  paragraph.innerHTML = message;
}
