import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Say Hello';
  name = 'Toni Whitfield';
  number = 2.3456;

  sayHello(name: string) {
    this.name = name;
    console.log(name);
  }

}




