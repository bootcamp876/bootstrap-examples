# Toni Whitfield

I am a Software Developer who lives in Michigan.  I have been programming with the Progress programming language since
1996.  I'm currently in a Full Stack Developer bootcamp, hoping to learn some new programming languages.

* Scrapbooking
* Researching Family History
* Traveling

1. OpenEdge
2. Visual Studio
3. HTML

```
<!DOCTYLE html>
<html lang="en">
 <head>
 <title>Cloning a Respository</title>
 </head>
<body>
 <p>To clone my respository, click [here](https://gitlab.com/Bootcamp876/git-exercise/Repository)</p>
 </body>
</html>
```

![Image](MackinacBridge.jpg "Mackinac Bridge")
