window.onload = function () {
    const btn = document.getElementById("callAPI");
    const postBtn = document.getElementById("postBtn");
    const submitCourse = document.getElementById("newCourse");
    submitCourse.onsubmit = onSubmitCourse;
    btn.onclick = onClickButton;
    postBtn.onclick = onPostButtonClick;
};

function postCourse() {
    let postCourseURL = "http://localhost:8081/api/coursesform";
    debugger;

    const courseForm = document.querySelector("form");
    const formData = new FormData(courseForm);
    
    fetch(postCourseURL, {
        method: "POST",
        body: formData
    })
    .then((response) => {
        if (response.status >= 200 && response.status < 300) {
            let confirmationMessage = document.getElementById("confirmationMessage");
            confirmationMessage.innerHTML = "Course added";
        } else {
            let confirmationMessage =
                document.getElementById("confirmationMessage");
                confirmationMessage.innerHTML = "Error returned with status code: " + response.status;
        }
        response.json()})
        .then(json => {
            debugger;
            console.log(json);
        })
        .catch(err => {
            console.log(err);
            let confirmationMessage = document.getElementById("confirmationMessage");
            confirmationMessage.innerHTML = "Unexpected error: " + err;
        });
        return false;
}

function onSubmitCourse() {
    let data = new FormData(document.getElementById("myCourse"));

    console.log(...data);
    // Send the request. Do not call JSON.stringify. Do not
    // set HTTP header
    fetch("http://localhost:8081/api/coursesform/", {
        method: "POST",
        body: data
    })
        .then((response) => response.json())
        .then((json) => {
            console.dir(json);
        })
        .catch((err) => {
            console.dir(err);
            return false;
        });

    return false;
}

