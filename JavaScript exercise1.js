let customerId = "123456";
let custFirstName = "Johnny";
let custMiddleName = "Luke";
let custLastName = "BeGood";
let custGender = "male";

let custDob = new Date("1900-01-01");  
//custDob.setFullYear(2011, 6, 1);
//custDob.toLocaleDateString(1900, 1, 1);

let custDriverLicense = "B123456789";
let custAutoPolicyNumber = "99999999";

/*
var customerId = "123456";
var custFirstName = "Johnny";
var custMiddleName = "Luke";
var custLastName = "BeGood";
var custGender = "male";

custDob = new Date();  
custDob.setFullYear(2011, 6, 1);

var custDriverLicense = "B123456789";
var custAutoPolicyNumber = "99999999";
*/

console.log(customerId);
console.log(custFirstName);
console.log(custMiddleName);
console.log(custLastName);
console.log(custGender);
console.log(custDob);
console.log(custDriverLicense);
console.log(custAutoPolicyNumber);

