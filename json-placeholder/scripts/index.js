"use strict";

window.onload = function () {
    const btn = document.getElementById("callAPI");

    let element = document.getElementById("messageArea");
    let url = "https://jsonplaceholder.typicode.com/todos/1";

    fetch(url)
        .then((response) => response.json())
        .then((data) => {
            let message = "User Id: " + data.id + " " + "Title: " + data.title;
            element.innerHTML = message;
        });
};