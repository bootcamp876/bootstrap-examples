"use strict";

window.onload = function(){
    const tbl1 = document.getElementById("mytable");
    let url = "https://rapidapi.com/thecocktaildb/api/the-cocktail-db/";   

    fetch(url)
    .then((response) => response.json())
    .then((data) => {
        for (let i = 0; i < data.length; i++) {
            let tblObject = tbl1.insertRow(-1);
            tblObject.scope = "col"; 
            
            let cell1 = tblObject.insertCell(0);
            cell1.innerHTML = data[i].name;

            let cell2 = tblObject.insertCell(1);
            cell2.innerHTML = data[i].username;

            let cell3 = tblObject.insertCell(2);
            cell3.innerHTML = data[i].email;

            let cell4 = tblObject.insertCell(3);
            cell4.innerHTML = data[i].phone;

            let cell5 = tblObject.insertCell(4);
            cell5.innerHTML = data[i].website;

            let cell6 = tblObject.insertCell(5);
            cell6.innerHTML = data[i].company.name;
        }
    });
};