import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Service } from '../models/service.model';
import { SpaService } from '../providers/spa.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})

export class ServicesComponent {
  title = "Spa Services";

  services: Array<Service> = [];

  constructor(
    private activatedRoute: ActivatedRoute, 
    private SpaService: SpaService) { }

  ngOnInit(): void {
    this.SpaService.getServices().subscribe((data) => {
      this.services = data;
    });
  }

}
