export class Categories {
    // properties
    public CategoryName: string = '';
    public CatId: string = '';
    public Image: string = '';
    

    constructor(CategoryName: string, CatId: string, Image: string) {
        this.CategoryName = CategoryName;
        this.CatId = CatId;
        this.Image = Image; 
    }   
}
