export class Reviews {
    // constructor(public date: string, public reviewer: string, public comment: string){}

    public date: string = '';
    public reviewer: string = '';
    public comment: string = '';

    constructor(date: string, reviewer: string, comment: string) {
        this.date = date;
        this.reviewer = reviewer; 
        this.comment = comment;
    }    
    
}

