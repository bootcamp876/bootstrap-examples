import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; 
import { Reviews } from '../models/reviews.model';
import { ReviewsService } from '../providers/reviews.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})

export class ReviewsComponent {
  title = "Reviews";

  reviews: Array<Reviews> = [];

  constructor(private activatedRoute: ActivatedRoute, private reviewsService: ReviewsService) { }

  ngOnInit(): void {
    this.reviewsService.getReviews().subscribe((data) => {
      this.reviews = data;
    });
  } 

}
