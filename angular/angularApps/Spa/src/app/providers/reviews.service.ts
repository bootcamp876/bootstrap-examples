import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Reviews } from '../models/reviews.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {
  private serviceEndpoint: string = 'http://localhost:8081/api/reviews';

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),

  };
  getReviews(): Observable<Reviews[]> {
    return this.http
      .get('http://localhost:8081/api/reviews', this.httpOptions)
      .pipe(map(res => <Reviews[]>res));
  }
}
