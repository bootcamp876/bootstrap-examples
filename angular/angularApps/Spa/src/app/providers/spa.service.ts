import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Service } from '../models/service.model';

@Injectable({
  providedIn: 'root'
})
export class SpaService {

  constructor(private http: HttpClient) { }
    private serviceEndpoint: string = 'http://localhost:8081/api/services';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getServices(): Observable<Service[]> {
    return this.http.get(this.serviceEndpoint, this.httpOptions)
      .pipe(map(res => <Service[]>res));
  }

  getServiceByCategory(CategoryID: string): Observable<Service[]> {
    return this.http.get(this.serviceEndpoint + '/bycategory/' + CategoryID, this.httpOptions)
      .pipe(map(res => <Service[]>res));
  }
}