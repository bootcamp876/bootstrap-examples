import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; 
import { Spa } from '../models/spa.model';
import { SpaService } from '../providers/spa.service';

@Component({
  selector: 'app-spa',
  templateUrl: './spa.component.html',
  styleUrls: ['./spa.component.css']
})

export class SpaComponent {
  
  constructor(private activatedRoute: ActivatedRoute, private ToDoService: SpaService) { }

}
