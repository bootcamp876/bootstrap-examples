import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Spa } from '../models/spa.model';
import { SpaService } from '../providers/spa.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})

export class categories {
  title = "Categories";

  spa: Array<Spa> = [];

  constructor(private activatedRoute: ActivatedRoute, private SpaService: SpaService) { }

  ngOnInit(): void {
    this.SpaService.getServices().subscribe((data) => {
      this.spa = data;
    });
  }
  
}
