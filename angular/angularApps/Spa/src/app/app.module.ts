// import modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

// import services
import { SpaService } from './providers/spa.service';

// import components
import { AppComponent } from './app.component';
import { SpaComponent } from './spa/spa.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { ServicesComponent } from './services/services.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { categories } from './categories/categories.component';
import { DetailsComponent } from './details/details.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "spa", component: SpaComponent },
  { path: "home", component: HomeComponent },
  { path: "search", component: SearchComponent},
  { path: "services", component: ServicesComponent},
  { path: "reviews", component: ReviewsComponent},
  { path: "categories", component: categories},

]

@NgModule({
  declarations: [
    AppComponent,
    SpaComponent,
    HomeComponent,
    SearchComponent,
    ServicesComponent,
    ReviewsComponent,
    DetailsComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule,
  ],
  providers: [SpaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
