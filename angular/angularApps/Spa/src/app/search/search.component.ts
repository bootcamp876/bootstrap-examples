import { Component, OnInit } from '@angular/core';
import { Categories } from '../models/categories.model';
import { Service } from '../models/service.model';
import { CategoriesService } from '../providers/categories.service';
import { SpaService } from '../providers/spa.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {
  title = "Services";

  categories: Array<Categories> = [];
  services: Array<Service> = [];

  constructor(
    private categoriesService: CategoriesService,
    private SpaService: SpaService) { }

  ngOnInit(): void {
    this.categoriesService.getCategories().subscribe(data => {
      this.categories = data;
    });
  }

  onSelectSpa(event: any): void {
    this.SpaService.getServiceByCategory(event.target.value).subscribe(data => {
      this.services = data;
    });
  }
}
