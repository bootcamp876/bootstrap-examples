import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Service } from '../models/service.model';
import { DetailsService } from '../providers/details.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent {
  title = "Treatment Details";

  /* details: Array<Details> = [];
 
  constructor(
    private activatedRoute: ActivatedRoute, 
    private DetailsService: DetailsService) { }

  ngOnInit(): void {
    this.DetailsService.getDetails().subscribe((data) => {
      this.details = data;
    });
  }  */
}
