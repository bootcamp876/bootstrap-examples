import { Component, OnInit } from '@angular/core';
import { Courses } from '../models/courses.model';
import { CoursesService } from '../providers/courses.service';
@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  title: string = "Courses Table";
  courses!: Array<any>;
  detailsUrl: string =
  "/course-details/";
  //"/course-details.html?courseid=";
  constructor(private coursesService: CoursesService) {
  }
  ngOnInit(): void {
    // call getEmployees() method in EmployeeService
    this.coursesService.getCourses().subscribe(data => {
      this.courses = data;
    });
  }

}

