import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CoursesService } from './providers/courses.service';

import { AppComponent } from './app.component';
import { CoursesComponent } from './courses/courses.component';
// import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
// import { HeaderComponent } from './header/header.component';
import { CourseDetailsComponent } from './course-details/course-details.component';

const appRoutes: Routes = [
  /* { path: "", component: HomeComponent},
  { path: "home", component: HomeComponent}, */
  { path: "courses", component: CoursesComponent },
  { path: "course-details/:id", component: CourseDetailsComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    /* HomeComponent,
    HeaderComponent, */
    CourseDetailsComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule, RouterModule.forRoot(appRoutes)
  ],
  providers: [CoursesService],
  bootstrap: [AppComponent]
})
export class AppModule { }

