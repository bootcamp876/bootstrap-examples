import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Courses } from '../models/courses.model';
import { CoursesService } from '../providers/courses.service';
@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit {
  course!: any;
  courseID!: number;
  // instructor!: string;
  // startDate!: string;
  // numDays!: string;

  constructor(private coursesService: CoursesService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.courseID = +params['id'];
        }
      );

    this.coursesService.getCourseDetails(this.courseID).subscribe(data => {
      this.course = data;
    });
  }

}

