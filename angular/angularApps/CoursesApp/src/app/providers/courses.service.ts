import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Courses } from '../models/courses.model';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  constructor(private http: HttpClient) { }

  private coursesUrl: string =
  'http://localhost:8081/api/courses';

  private httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json'
    })
    };

  getCourses() : Observable<any> {
    debugger;
    return this.http.get(this.coursesUrl,
    this.httpOptions).pipe(map(res => <Courses[]>res));
    }

    getCourseDetails(id: number) : Observable<Courses> {
      return this.http.get(this.coursesUrl + '/' + id,
        this.httpOptions).pipe(map(res => <Courses[]>res));
      }
}

