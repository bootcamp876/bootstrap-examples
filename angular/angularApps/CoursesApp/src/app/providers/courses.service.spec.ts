import { TestBed } from '@angular/core/testing';

import { CourseService } from './courses.service';

describe('CourseService', () => {
  let service: CourseServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CourseServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
