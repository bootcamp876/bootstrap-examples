import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ToDosComponent } from './to-dos/to-dos.component';
import { ToDoService } from './providers/todo.service';
import { HeaderComponent } from './header/header.component';
import { UsersComponent } from './users/users.component';
import { CoursesComponent } from './courses/courses.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "todos", component: ToDosComponent },  
  { path: "users", component: UsersComponent }, 
  { path: "courses", component: CoursesComponent },

]

@NgModule({
  declarations: [
    AppComponent,
    ToDosComponent,
    HeaderComponent,
    UsersComponent,
    CoursesComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
  ],
  providers: [ToDoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
