/* import { Component } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {

} */

import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Users } from '../models/users.model';
import { UserService } from '../providers/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent {
  users: Array<Users> = [];

  constructor(private activatedRoute: ActivatedRoute, private UserService: UserService) { }

  ngOnInit(): void {
    this.UserService.getUsers().subscribe((data) => {
      this.users = data;
    });
  }
}
