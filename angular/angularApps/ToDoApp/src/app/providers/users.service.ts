import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Users } from '../models/users.model';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  constructor(private http: HttpClient) {}
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };

  getUsers(): Observable<Users[]> {
    return this.http
      .get('https://jsonplaceholder.typicode.com/users/', this.httpOptions)
      .pipe(map((res) => <Users[]>res));
  }
}