import { Component, OnInit } from '@angular/core';
import { Bookmodel } from '../models/bookmodel.model';

@Component({
  selector: 'app-bookmodel',
  templateUrl: './bookmodel.component.html',
  styleUrls: ['./bookmodel.component.css']
})
export class BookmodelComponent {
  title = 'bookmodel';
  categories: Array<string> = [];
  books: Array<Bookmodel> = [];

  constructor() {
    this.categories =
      ["Title", "Author", "ISBN"];

    this.books = [
      new Bookmodel("Disney", "Walt Disney", "1234abc"),
      new Bookmodel("Animal Kingdom", "Walt Disney", "5678defg"),
      new Bookmodel("Jurassic Park", "Steven Spielberg", "3456hijk")
  
    ];
  }

}
