import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmodelComponent } from './bookmodel.component';

describe('BookmodelComponent', () => {
  let component: BookmodelComponent;
  let fixture: ComponentFixture<BookmodelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookmodelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BookmodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
