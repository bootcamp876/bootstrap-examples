import { Component } from '@angular/core';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent {
  name1 : string = "Mickey Mouse";
  email1 : string = "mickey.mouse@cartoons.com";
  phone1 : string = "555-55-5555"; 

  name2 : string = "Donald Duck";
  email2 : string = "donald.duck@cartoons.com";
  phone2 : string = "444-55-5555"; 

  name3 : string = "Minnie Mouse";
  email3 : string = "minnie.mouse@cartoons.com";
  phone3 : string = "333-55-5555"; 

}
