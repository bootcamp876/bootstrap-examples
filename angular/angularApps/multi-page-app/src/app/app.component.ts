import { Component } from '@angular/core';
@Component({
 selector: 'app-root',
 templateUrl: './app.component.html',
 styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Math Calculator';

    operand1: number = 0;
    operand2: number = 0;
    answer: number = 0;
    answerIsAvailable: boolean = false;
  
    onClearClicked(): void {
      this.operand1 = 0;
      this.operand2 = 0;
      this.answer = 0;
  
    }
  
    onAddClicked(): void {
      this.answer = this.operand1 + this.operand2;
      this.answerIsAvailable = true;
    }
  
    onSubtractClicked(): void {
      this.answer = this.operand1 - this.operand2; 
      this.answerIsAvailable = true;
      
    }
  
    onMultiplyClicked(): void {
      this.answer = this.operand1 * this.operand2; 
      this.answerIsAvailable = true;
    }
  
    onDivideClicked(): void {
      this.answer = this.operand1 / this.operand2; 
      this.answerIsAvailable = true;
      
    }
 }


