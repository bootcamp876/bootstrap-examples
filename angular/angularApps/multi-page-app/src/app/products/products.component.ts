import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
  product1ImageURL: string = "assets/Deer.jpg";
  product1AltText: string = "Deer";

  product2ImageURL: string = "assets/MackinacBridge.jpg";
  product2AltText: string = "Mackinaw Bridge";

  product3ImageURL: string = "assets/MackinawCity.jpg";
  product3AltText: string = "Mackinaw City";

}
