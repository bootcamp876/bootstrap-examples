import { Component } from '@angular/core';

@Component({
  selector: 'app-word-counter',
  templateUrl: './word-counter.component.html',
  styleUrls: ['./word-counter.component.css']
})
export class WordCounterComponent  {
  title: string = "Word Counter";
  inputText!: string;
  wordCount: number = 0;

  onSubmit(): void {
    console.log (this.inputText);
    this.wordCount = this.CountWords(this.inputText)
  }
  
  CountWords(inputText: string): number {
    return inputText.split(" ").length;
  }
  
}

