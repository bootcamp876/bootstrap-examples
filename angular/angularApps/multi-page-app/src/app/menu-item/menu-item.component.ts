import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})

export class MenuItemComponent {

  menuItems = [
    {
      id: 1, name: 'Burger',
      description: 'A classic hamburger',
      price: 5.99,
    },
    {
      id: 2, name: 'Fries',
      description: 'Crispy french fries',
      price: 2.99,
    },
    {
      id: 3, name: 'Soda',
      description: 'A cold soda',
      price: 1.99,
    },
  ];
 
  menuItem!: any;
  name!: any;
  description!: any;
  price!: any;

  constructor(
    private ActivatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.ActivatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];
      this.menuItem = this.getMenuItems(id);
  
    });
  }
  getMenuItems(id: number) {
    return this.menuItems.find((menuItem) => menuItem.id == id);
  }
}