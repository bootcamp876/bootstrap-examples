import { Component } from '@angular/core';
import { Router } from '@angular/router';
// import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent {
  menuItems = [
    {
      id: 1, name: 'Burger',
      description: 'A classic hamburger',
      price: 5.99
    },
    {
      id: 2, name: 'Fries',
      description: 'Crispy french fries',
      price: 2.99
    },
    {
      id: 3, name: 'Soda',
      description: 'A cold soda',
      price: 1.99
    }
  ];
  menu!: any;

  constructor(private router: Router) { }

  getDetails(menuId: number) {
    console.log(menuId);
    debugger;
    this.router.navigate(['/list'],
      {
        queryParams: {
          id: menuId
        }
      });
  }

}



