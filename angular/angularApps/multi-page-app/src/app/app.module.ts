import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { MenuComponent } from './menu/menu.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ProductsComponent } from './products/products.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { UserInputComponent } from './user-input/user-input.component';
import { WordCounterComponent } from './word-counter/word-counter.component';
import { UserComponent } from './InputOutputExercises/user/user.component';
import { BookComponent } from './book/book.component';
import { OutdoorActivitiesComponent } from './outdoor-activities/outdoor-activities.component';
import { BookmodelComponent } from './bookmodel/bookmodel.component';
import { ContactComponent } from './contact/contact.component';


const appRoutes: Routes = [
/*   { path: 'song', component: PlayerComponent },
  { path: 'list', component: PlaylistComponent } */
  { path: 'menu', component: MenuComponent },
  { path: 'list', component: MenuItemComponent },
  { path: 'contact-us', component: ContactUsComponent},
  { path: 'home', component: HomeComponent},
  { path: 'products', component: ProductsComponent},
  { path: 'user-input', component: UserInputComponent},
  { path: 'word-counter', component: WordCounterComponent},
  { path: 'outdoor-activities', component: OutdoorActivitiesComponent},
  { path: 'bookmodel', component: BookmodelComponent},
  { path: 'contact', component: ContactComponent}
  
];


@NgModule({
  declarations: [AppComponent, MenuItemComponent, MenuComponent, UserInputComponent, WordCounterComponent, 
    UserComponent, BookComponent, OutdoorActivitiesComponent, BookmodelComponent, ContactComponent],
  imports: [BrowserModule, RouterModule.forRoot(appRoutes), FormsModule],
  bootstrap: [AppComponent],
  providers: [],
 })
 
 export class AppModule { }


/* import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component'; 

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component'; import { HeaderComponent } from './header/header.component';
import { ShowPersonComponent } from './show-person/show-person.component';
import { ItemListComponent } from './item-list/item-list.component';
import { UpdateExerciseComponent } from './update-exercise/update-exercise.component';
import { TestComponent } from './header/test/test.component'; 
import { HomeComponent } from './home/home.component';
import { DifferentComponent } from './different/different.component';
import { ProductsComponent } from './products/products.component';
import { ContactUsComponent } from './contact-us/contact-us.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  // { path: 'different', component: DifferentComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    ShowPersonComponent,
    ItemListComponent,
    UpdateExerciseComponent,
    TestComponent, 
    DifferentComponent,
    ProductsComponent,
    ContactUsComponent,
  ], 
  imports: [BrowserModule, RouterModule.forRoot(appRoutes)],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {} */

