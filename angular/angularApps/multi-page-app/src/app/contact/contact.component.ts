import { Component } from '@angular/core';
import { Contact } from '../models/contact.model';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  contacts: Array<Contact> = [];
  newContact: Contact = new Contact ("", "", "") ;

  constructor() {}

 /*  ngOnInit(): void {
    this.contact = [
      new Contact('Steven Spielberg', 'steven.spielberg@et.com', '555-55-5555'),
      new Contact('Donald Duck', 'donald.duck@disney.com', '444-55-5555'),
      new Contact('Adam West', 'adam.westg@batman.com', '333-55-5555'),

    ];
  } */

  onAddContactSubmit() {
    this.contacts.push(this.newContact);
    this.newContact = new Contact('', '', '');
  }
}