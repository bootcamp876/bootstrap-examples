import { Component, OnInit } from '@angular/core';
import { Saying } from './models/saying.model';
import { SayingsService } from './providers/sayings.service';
import { RandomService } from './providers/random.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  // title = 'Sayings';
  title = 'Random Number Service';
  randomNumber!: Number;
  constructor(private randomService: RandomService) {
  }
  ngOnInit(): void {
    this.randomNumber = this.randomService.getRandomNum();
  }


  
  /* // this is the data the HTML template binds to
  categories: Array<string> = [];
  // this is the matching sayings the HTML template binds to
  matchingSayings: Array<Saying> = [];

  hasSayings!: boolean;

  // receive the SayingsService we need to make calls to
   constructor(private sayingsService: SayingsService) {
   }
  // when the component loads, get the categories
   ngOnInit() {
    this.categories = this.sayingsService.getCategories();
    console.dir(this.categories);
  }
  
  // when they pick from the dropdown, get the matching sayings
  onSelectSaying(event: any): void {
    const selectedCategory = event.target.value;
    // if they pick the select one option... show nothing
    if (selectedCategory == "") {
      this.matchingSayings = [];
      this.hasSayings = true;
    }
    else {
      // otherwise... find the matching sayings and show
      this.matchingSayings =
        this.sayingsService.getSayingsThatMatchCategory(
          selectedCategory);
      debugger;
      if (this.matchingSayings.length > 0) {
        this.hasSayings = true;
      } else {
        this.hasSayings = false;
      }

    }
  }

  getSayingsByPerson(personInput: string) {
    this.matchingSayings = this.sayingsService.getSayings().filter(s => s.person == personInput);

    if (this.matchingSayings.length > 0) {
      this.hasSayings = true;
    } else {
      this.hasSayings = false;
    }
  }

  clearValues(personInput: any, selectInput: any){
    personInput.value = '';
    selectInput.value = '';
    this.matchingSayings = [];
    this.hasSayings = true;
  } */



}



