import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomService {

  private randomNumber!: Number;

  constructor() { }

  public getRandomNum(): Number {
    this.randomNumber = Math.round(Math.random()*100);
    return this.randomNumber;
  }

}

