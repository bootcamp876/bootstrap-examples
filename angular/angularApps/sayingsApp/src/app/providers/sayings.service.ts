import { Injectable } from '@angular/core';
import { Saying } from '../models/saying.model';

@Injectable({
  providedIn: 'root'
})

export class SayingsService {
  private categories: Array<string>;
  private sayings: Array<Saying>;

  constructor() {
    // load the categories
    this.categories = ["Inspiration", "Money", "Staying Safe", "Statistics"];

    // load the sayings
    this.sayings = [
      new Saying('Inspiration', 'You\'re braver than you believe, and stronger than you seem, and smarter than you think.', 'Mickey Mouse'),
      new Saying('Inspiration', 'Nothing is particularly hard if you break it down into small jobs.', 'Donald Duck'),
      new Saying('Staying Safe', 'An apple a day keeps the doctor away.', 'Mark Twain'),
      new Saying('Staying Safe', 'A ship in a harbor is safe, but that is not what ships are for.', 'Maya Angelou'),
      new Saying('Statistics', 'He uses statistics as a drunken man uses lamp posts... for support rather than illumination.', 'Steven Spielberg')
    ];

  }

  // Methods callable from anywhere in the Angular app
  public getCategories(): Array<string> {
    return this.categories;
  }
  public getSayings(): Array<Saying> {
    return this.sayings;
  }
  public getSayingsThatMatchCategory(category: string)
    : Array<Saying> {
    let matching = this.sayings.filter(s => s.category === category);
    return matching;
  }

}



