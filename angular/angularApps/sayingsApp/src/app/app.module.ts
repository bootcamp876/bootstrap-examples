import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SayingsService } from './providers/sayings.service';
import { AppComponent } from './app.component';
import { DisplayRandomComponent } from './display-random/display-random.component';
import { RandomService } from './providers/random.service';

/* import { HomeComponent } from './home/home.component';
import { SayingComponent } from './saying/saying.component'; */

const appRoutes: Routes = [
  /* { path: '', component: SayingsService } */
  { path: '', component: RandomService }
];

@NgModule({
  declarations: [
    AppComponent,
    DisplayRandomComponent,
    
   /* HomeComponent, 
      SayingComponent */
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  // providers: [SayingsService],
  providers: [RandomService],
  bootstrap: [AppComponent]
})
export class AppModule { }


