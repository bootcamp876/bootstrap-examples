import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
//import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AnimalComponent } from './animal/animal.component';
import { HomeComponent } from './home/home.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

import { AnimalsService } from './providers/animals.service';
import { AnimalDetailsComponent } from './animal-details/animal-details.component';

const appRoutes: Routes = [  
  { path: "", component: HomeComponent },
  { path: "Animals", component: AnimalComponent},
  { path: "detail", component: AnimalDetailsComponent},
 // { path: "newanimal", component: NewanimalComponent },

];


@NgModule({
  declarations: [
    AppComponent,
    AnimalComponent,
    HomeComponent,
    NavBarComponent,
    AnimalDetailsComponent
  ],
  imports: [
    BrowserModule,    
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
