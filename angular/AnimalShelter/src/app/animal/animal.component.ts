import { Component, OnInit } from '@angular/core';
import { Animals } from '../models/animals.model';
import { AnimalsService } from '../providers/animals.service';


@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})

export class AnimalComponent{
  title = 'Loving Care Animal Shelter';
  animals!: Array<any>; 
  detailsUrl: string = "/animal-details/"; 

  constructor(private animalsService: AnimalsService){

  }

  
   ngOnInit() {
    this.animalsService.getAnimals().subscribe(data => {this.animals = data;}); 
   
    }  

}
