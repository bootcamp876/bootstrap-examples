import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Animals } from '../models/animals.model';

@Injectable({
  providedIn: 'root'
})
export class AnimalsService {

  
    url = "https://localhost:7034/api/";
    constructor(private http: HttpClient) { }
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      // 'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };
  getAnimals(): Observable<Animals[]> {
    return this.http
      .get(this.url + "Animals", this.httpOptions)
      .pipe(map((res) => <Animals[]>res));
  }

}


