import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'Loving Care Animal Shelter';
  today = "";

  ngOnInit(){
    this.today = new Date().toLocaleDateString();
  }
}
