import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Animals } from '../models/animals.model';
import { AnimalsService } from '../providers/animals.service';

@Component({
  selector: 'app-animal-details',
  templateUrl: './animal-details.component.html',
  styleUrls: ['./animal-details.component.css']
})
export class AnimalDetailsComponent {
  animals!: any;
  animalID!: number;
  name!: string;
  species!: string;
  breed!: string;
  owner!: string;
  checkindate!: Date;
  checkoutdate!: Date;

  constructor(private animalsService: AnimalsService, private route: ActivatedRoute) {
  } 
/* 
    ngOnInit(): void {
      this.route.params
        .subscribe(
          (params: Params) => {
            this.animalID = +params['animal.id'];
          }
        );

      this.animalsService.getAnimalDetails(this.animalID).subscribe(data => {this.animals = data;});
    } */
 

} //export class
