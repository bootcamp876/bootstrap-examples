import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'first-app';
  today: string = "";
  greeting: string = "";

  ngOnInit() {
    let rightNow = new Date();
    this.today = rightNow.toLocaleDateString();
    
    if (rightNow.getHours() >=5 && rightNow.getHours() <= 11){
      this.greeting = "It's a wonderful morning";
    } else if (rightNow.getHours() >= 12 && rightNow.getHours() <= 17){
      this.greeting = "It's a great afternoon";
    } else if (rightNow.getHours() >= 18 && rightNow.getHours() <= 23){
      this.greeting = "It's a nice evening";
    }else if (rightNow.getHours() >=0 && rightNow.getHours() <= 4){
      this.greeting = "It's deep night - why are yu awake?";
    }
  }
 
}
