/* import { Component } from '@angular/core';

@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css']
})
export class MyComponentComponent {
  title = 'My Custom Component';
  constructor() { }
  ngOnInit(): void {

  }
} */

import { Component, OnInit } from '@angular/core';
@Component({
 selector: 'app-my-component',
 templateUrl: './my-component.component.html',
 styleUrls: ['./my-component.component.css']
})
export class MyComponentComponent implements OnInit {
 title = 'My Custom Component';
 constructor() { }
 ngOnInit(): void {
 }
}


