import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditsuperheroComponent } from './editsuperhero.component';

describe('EditsuperheroComponent', () => {
  let component: EditsuperheroComponent;
  let fixture: ComponentFixture<EditsuperheroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditsuperheroComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditsuperheroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
