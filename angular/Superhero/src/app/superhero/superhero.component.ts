import { Component } from '@angular/core';
import { Superhero } from '../models/superhero.model';
import { ActivatedRoute } from '@angular/router';
import { SuperheroService } from '../providers/superhero.service'; 

@Component({
  selector: 'app-superhero',
  templateUrl: './superhero.component.html',
  styleUrls: ['./superhero.component.css']
})
export class SuperheroComponent { 
  title = 'Superhero Headquarters';
  superheroes: Array<Superhero> = [];
  powerId: number = 0;

  constructor(private activatedRoute: ActivatedRoute, private superheroService: SuperheroService) { 

  }

  ngOnInit(){
    this.superheroService.getSuperheroes().subscribe(data => {this.superheroes = data;});
    
  } 

  /* onClickSearchButton() {
    this.superheroService.getSuperheroesByPower(this.powerId).subscribe((data) => {this.superheroes = data;});
  
  } */
}
