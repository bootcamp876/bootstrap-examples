import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Superhero } from '../models/superhero.model';
import { SuperheroService } from '../providers/superhero.service';


@Component({
  selector: 'app-deletesuperhero',
  templateUrl: './deletesuperhero.component.html',
  styleUrls: ['./deletesuperhero.component.css']
})
export class DeletesuperheroComponent {
  superhero!: Superhero;
  id: number = 0;
  name: string = "";
  nickname: string = "";

  constructor(private activatedRoute: ActivatedRoute, private SuperheroService: SuperheroService, private router: Router) {
  }

  

}
