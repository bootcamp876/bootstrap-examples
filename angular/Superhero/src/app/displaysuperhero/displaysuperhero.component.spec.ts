import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplaysuperheroComponent } from './displaysuperhero.component';

describe('DisplaysuperheroComponent', () => {
  let component: DisplaysuperheroComponent;
  let fixture: ComponentFixture<DisplaysuperheroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplaysuperheroComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplaysuperheroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
