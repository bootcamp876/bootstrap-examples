import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Superhero } from '../models/superhero.model';

@Injectable({
  providedIn: 'root'
})
export class SuperheroService {
 
  url = "https://localhost:7246/api/";
  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      // 'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };
  getSuperheroes(): Observable<Superhero[]> {
    return this.http
      .get(this.url + "Superhero", this.httpOptions)
      .pipe(map((res) => <Superhero[]>res));
  }

  
}
