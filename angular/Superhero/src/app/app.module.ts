import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { SuperheroComponent } from './superhero/superhero.component';
import { AddsuperheroComponent } from './addsuperhero/addsuperhero.component';
import { EditsuperheroComponent } from './editsuperhero/editsuperhero.component';
import { DeletesuperheroComponent } from './deletesuperhero/deletesuperhero.component';
import { DisplaysuperheroComponent } from './displaysuperhero/displaysuperhero.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SuperheroDetailsComponent } from './superhero-details/superhero-details.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { SuperheroService } from './providers/superhero.service';
import { SuperpowerComponent } from './superpower/superpower.component';

const appRoutes: Routes = [  
  { path: '', component: HomeComponent },
  { path: 'Superhero', component: SuperheroComponent},
  { path: 'detail', component: SuperheroDetailsComponent},
 // { path: 'add', component: AddSuperheroComponent },
 // { path: 'edit', component: EditSuperHeroComponent },
 // { path: 'delete' component: DeleteSuperHeroComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    SuperheroComponent,
    AddsuperheroComponent,
    EditsuperheroComponent,
    DeletesuperheroComponent,
    DisplaysuperheroComponent,
    HomeComponent,
    NavbarComponent,
    SuperheroDetailsComponent,
    SuperpowerComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
  ],
  providers: [ SuperheroService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
