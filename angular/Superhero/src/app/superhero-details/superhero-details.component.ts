import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Superhero } from '../models/superhero.model';
import { SuperheroService } from '../providers/superhero.service';

@Component({
  selector: 'app-superhero-details',
  templateUrl: './superhero-details.component.html',
  styleUrls: ['./superhero-details.component.css']
})
export class SuperheroDetailsComponent {
  superheroes!: any;
  Id!: number;
  name!: string;
  nickname!: string;
  superpower!: string;
  telephonenumber!: string;
  dateofbirth!: Date;

  constructor(private superheroService: SuperheroService, private route: ActivatedRoute) {
  }
  
}
