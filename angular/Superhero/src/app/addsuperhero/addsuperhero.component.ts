import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Superhero } from '../models/superhero.model';
import { SuperheroService } from '../providers/superhero.service';


@Component({
  selector: 'app-addsuperhero',
  templateUrl: './addsuperhero.component.html',
  styleUrls: ['./addsuperhero.component.css']
})
export class AddsuperheroComponent {
  superhero!: Superhero;
  name: string = "";
  nickname: string = "";
  superpower: string = "";
  telephoneNumber: string = "";
  //dateOfBirth: Date = ;

  constructor(private activatedRoute: ActivatedRoute, private SuperheroService: SuperheroService, private router: Router){

  }

  /* onClickAddButton() {
    this.SuperheroService.postNewSuperhero(new Superhero(0, this.name, this.nickname, this.telephoneNumber, this.dateOfBirth)).subscribe(data => console.dir(data));
    this.router.navigate(['/superheroes']);
  } */
}
