import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsuperheroComponent } from './addsuperhero.component';

describe('AddsuperheroComponent', () => {
  let component: AddsuperheroComponent;
  let fixture: ComponentFixture<AddsuperheroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddsuperheroComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddsuperheroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
