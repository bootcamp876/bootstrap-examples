
let teams = [
  { code: "DAL", name: "Dallas Cowboys", plays: "Arlington, TX" },
  { code: "DEN", name: "Denver Broncos", plays: "Denver, CO" },
  { code: "HOU", name: "Houston Texans", plays: "Houston, TX" },
  { code: "KAN", name: "Kansas City Chiefs", plays: "Kansas City, MO" },
];

window.onload = function () {
  initiateDropdownStates();
  initiatePlayBtn();
};

function initiateDropdownStates() {
  const teamsList = document.getElementById("teamsList");

  for (let i = 0; i < teams.length; i++) {
    teamsList.appendChild(new Option(teams[i].name, teams[i].code));
  }
}

function initiatePlayBtn() {
  const btnPlays = document.getElementById("btnPlays");
  btnPlays.onclick = displayPlays;

}

function displayPlays() {
  const teamsList = document.getElementById("teamsList");
  let selectedValue = teamsList.value;

  if (selectedValue == null) {
    alert("No team was selected");
    return; // exit the event handler
  }
  else {
    const playArea = document.getElementById("playArea");
    alert("You selected " + selectedValue);

    playArea.innerHTML = "You selected the " + teams[teamsList.selectedIndex].name + " who play in " + teams[teamsList.selectedIndex].plays;
  }
}


