
window.onload = function () {
    initiateDropdownStates();
  };
  
  function initiateDropdownStates() {
    const statesList = document.getElementById("statesList");
    /* let states = [
      {code:"DAL", name:"Dallas Cowboys", plays:"Arlington, TX"},
      {code:"DEN", name:"Denver Broncos", plays:"Denver, CO"},
      {code:"HOU", name:"Houston Texans", plays:"Houston, TX"},
      {code:"KAN", name:"Kansas City Chiefs", plays:"Kansas City, MO"},
     ]; */

    let states = ["Alabama", "Alaska", "Arizona"];
    let abbrev = ["AL", "AK", "AZ"];
  
    for (let i = 0; i < states.length; i++) {
      statesList.appendChild(new Option(states[i], abbrev[i]));
    }
  }
  




