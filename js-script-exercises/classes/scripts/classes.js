class Employee {
    constructor(id, firstName, lastName, jobTitle, payRate) {
        this.employeeId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.jobTitle = jobTitle;
        this.payRate = payRate;
    }

    getFullName() {
        return this.firstName + " " + this.lastName;
    }
    // The getIntro() method returns a string
    getIntro() {
        let intro = "Hi! I'm " + this.getFullName() + " and I am a " + this.jobTitle;
        return intro;
    }

    promote(newJobTitle, newPayRate) {
        this.jobTitle = newJobTitle;
        this.payRate = newPayRate;
    }
}

/* let employee1 = new Employee(
    1, "Ian", "Auston", "Graphic Artist", 42.50);

console.log(`Employee ${employee1.firstName} created`);
console.log(`Job title is ${employee1.jobTitle}`);
console.log(`Pay rate is $${employee1.payRate}`);
*/

let employee2 = new Employee(
    2, "Snoop", "Dog", "Artist", 1000.00);

console.log(`Employee ${employee2.firstName} created`);
console.log(`Job title is ${employee2.jobTitle}`);
console.log(`Pay rate is $${employee2.payRate}`); 

let employee1 = new Employee(
    1, "Ian", "Auston", "Graphic Artist", 42.50);
   console.log(`Employee ${employee1.getFullName()} created`);

   employee1.promote("Sr. Graphic Artist", 46.75);
   console.log(`Job title is ${employee1.jobTitle}`);
   console.log(`Pay rate is $${employee1.payRate}`);

let intro = employee1.getIntro();
console.log(intro);

employee1.promote("Sr. Graphic Artist", 46.75);
console.log(`Job title is ${employee1.jobTitle}`);
console.log(`Pay rate is $${employee1.payRate}`)

let intro2 = employee2.getIntro();
console.log(intro2);

employee2.promote("Musician", 100.00);
console.log(`Job title is ${employee1.jobTitle}`);
console.log(`Pay rate is $${employee1.payRate}`)