// This is the Person class
class Person {
    constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    }

    getFullName() {
    return this.firstName + " " + this.lastName;
    }

    getIntro() {
    return "My name is " + this.getFullName() + "!";
    }
}
   
// This is the extended class for Person  
class Student extends Person {
    constructor(firstName, lastName, grade, major) {
        super(firstName, lastName);

        this.grade = grade
        this.major = major
    }

    // You can also use methods defined in the parent class
    // from the extended class
    getIntro() {
        return "My name is " + this.getFullName() +
            " and I am a " + this.grade +
            " and I study " + this.major + "!";
    }
}

// This is the extended class for Employee  
class Employee extends Person {
    constructor(firstName, lastName, id, jobTitle, payRate) {
        super(firstName, lastName);

        this.id = id
        this.jobTitle = jobTitle
        this.payRate = payRate
    }

    // You can also use methods defined in the parent class
    // from the extended class
    getIntro() {
        return "My name is " + this.getFullName();   
    }

    getGrossPay(hoursWorked) {
        return hoursWorked * this.payRate;
    }

}

let sallie = new Employee("Sallie", "Sheppard", 25, "Clerk", 50.00);
console.log(sallie.getFullName());
console.log(sallie.getGrossPay(40));

let tim = new Employee( "Tim", "Kellogg", 26, "Clerk", 55.00);
console.log(tim.getFullName());
console.log(tim.getGrossPay(40));


