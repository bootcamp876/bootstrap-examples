let courses = [
    {
    CourseId: "PROG100",
    Title: "Introduction to HTML/CSS/Git",
    Location: "Classroom 7",
    StartDate: "09/08/22",
    Fee: "100.00",
    },
    {
    CourseId: "PROG200",
    Title: "Introduction to JavaScript",
    Location: "Classroom 9",
    StartDate: "11/22/22",
    Fee: "350.00",
    },
    {
    CourseId: "PROG300",
    Title: "Introduction to Java",
    Location: "Classroom 1",
    StartDate: "01/09/23",
    Fee: "50.00",
    },
    {
    CourseId: "PROG400",
    Title: "Introduction to SQL and Databases",
    Location: "Classroom 7",
    StartDate: "03/16/23",
    Fee: "50.00",
    },
    {
    CourseId: "PROG500",
    Title: "Introduction to Angular",
    Location: "Classroom 1",
    StartDate: "04/25/23",
    Fee: "50.00",
    }
   ];

   //exercise 1
    function findProg200(arrayValue) {
        if (arrayValue.CourseId == "PROG200"){
            return true;
        }
        else {
            return false;
        }
    }

    //exercise 2
    function findProg500(arrayValue) {
        if (arrayValue.CourseId == "PROG500"){
            return true;
        }
        else {
            return false;           
        }

    }

    //exercise 3
    function getCoursesUnder50(courses){ 
        return coursesUnder50;
    }

    //exercise 4
    function getClassesInRoom1(courses){
        
        let classesInRoom1 = courses.filter(x => 
          x.Location === "Classroom 1");
       
        return classesInRoom1;
    }
 
    //exercise 1
 /*    let found = courses.find(findProg200);
    console.log(found.StartDate); */

    //exercise 2
    /* let found = courses.find(findProg500);
    console.log(found.StartDate); */

    //exercise 3
 /*    let found = courses.filter(x => Number(x.Fee) <= 50);
    console.log(found); */

    //exercise 4
    let found = getClassesInRoom1(courses);
    console.log(found);
   
