let menu = [
    [
        {item: "Sausage and Egg Biscuit", price: 3.69},
        {item: "Bacon and Egg Biscuit", price: 3.49},
        {item: "Ham and Egg Biscuit", price: 3.29}
    ],
    [
        {item: "Cheese Burger and Fries", price: 6.99},
        {item: "Caesar Salad and Fruit Cup", price: 10.99},
        {item: "Corned Beef sandwich and Chips", price: 16.99},
        {item: "Coney Dog and Chili Fries", price: 8.99}
    ],
    [ 
        {item: "Grilled Steak and Vegetables", price: 24.99},
        {item: "Chicken Fetticini Alfredo and Tossed Salad", price: 22.99},
        {item: "Pot Roast with Mashed Potatoes", price: 26.99},
        {item: "Spaghetti with Meatballs with Garlic Bread", price: 18.99},
        {item: "Fried Chicken with Greens, Sweet Potatoes", price: 21.99}
    ]
];
let meal = [
    [0],
    [1],
    [2]
];

let menuItem = 2;
displayMenu(menuItem);

function displayMenu(menuItem) {
    let title = ["Breakfast", "Lunch", "Dinner"];

    switch(menuItem){
        case 0:
            heading = "Breakfast Menu";
            break;
        case 1:
            heading = "Lunch Menu";
            break;
        case 2:
            heading = "Dinner Menu";
            break;
        default:
            heading = "No Menu Available";

    }

    console.log("--------------");
    console.log(heading);
    console.log("--------------");
    console.log(menu[menuItem]);

}



