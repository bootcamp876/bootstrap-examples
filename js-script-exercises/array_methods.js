let cart = [
    {item: "Bread", price: 3.29, quantity: 2},
    {item: "Milk", price: 4.09, quantity: 1},
    {item: "T-Bone Steak", price: 12.99, quantity: 2},
    {item: "Baking Potato", price: 1.89, quantity: 6},
    {item: "Iceberg Lettuce", price: 2.06, quantity: 1},
    {item: "Ice Cream - Vanilla", price: 6.81, quantity: 1},
    {item: "Apples", price: 0.66, quantity: 6}
   ];

 // map() function

 //use map() to return item name
 /* function buildCart(arrayElement){
    return arrayElement.item;
 }

 let cartList = cart.map(buildCart);
 let numElements = cartList.length;
 for (let i = 0; i < numElements; i++){
    console.log(cartList[i]);
 } 
 */

 //use forEach() to return list of items
 //let cartList.forEach(display)
/* function cartList (buildCart)
{
    return buildCart.item;
}

function displayCartItems(cartItems){
    console.log(cartItems);
}

 let cartItems = cart.map(cartList);

 cartItems.forEach(displayCartItems); */

//total cost of everything using reduce()

function getSum(currentTotal, arrayValue) {
    // adds the array value to the accumulated total
    return currentTotal + arrayValue;
   }
   let sum = numbers.reduce(getSum, 0);





