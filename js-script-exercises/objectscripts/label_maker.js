function printContact(emp1) {
    console.log(emp1.name);
    console.log(emp1.address);
    console.log(emp1.city, emp1.zip);
}

let emp1 = {
    name: "Toni Whitfield",
    address: "123 Main St",
    city: "Royal Oak, MI",
    // state: "MI",
    zip: "12345"
};

/* console.log("name: " + emp1.name);
console.log("address: " + emp1.address);
console.log("city: " + emp1.city);
console.log("state: " + emp1.state)
console.log("zip: " + emp1.zip); */

/* let emp1 = {
    name: "Toni",
    address: "123 Main St",
    city: "Royal Oak",
    state: "MI",
    zip: "12345"
}; */

printContact(emp1);
