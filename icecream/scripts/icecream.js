"use strict";

function processInput() {
    // get the input fields
    
    const numOfScoops = document.getElementById("numOfScoops");
    const toppings = document.getElementById("toppings");
    const basePrice = document.getElementById("basePrice");
    const tax = document.getElementById("tax");
    const total = document.getElementById("total");

    // get the values
    let userNumOfScoops = numOfScoops.value;
    let userToppings = toppings.value;
    let userBasePrice = basePrice.value;
    let userTax = tax.value;
    let userTotal = total.value;

    let springles = document.getElementById("sprinkles").checked;
    if (springles == true){
        toppings = .50;
    }

    let whipcream = document.getElementById("whipcream").checked;
    if (whipcream == true){
        toppings = .25;
    }

    let fudge = document.getElementById("fudge").checked;
    if (springles == true){
        toppings = 1.25;
    }

    let cherry = document.getElementById("cherry").checked;
    if (cherry == true){
        toppings = .25;
    } 
}