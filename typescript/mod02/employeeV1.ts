class Employee {
    private id: number;

    public name: string;
    public jobTitle: string;
    public payRate: number;

    constructor(id: number, name: string, jobTitle: string, payRate: number) {

        this.id = id;
        this.name = name;
        this.jobTitle = jobTitle;
        this.payRate = payRate;
    }
    toString(): string {
        return `${this.name} (${this.id}) is an ${this.jobTitle} earning $${this.payRate.toFixed(2)} /hr`;
    }
}

let emp1 = new Employee(1, "Ezra Aiden", "Astronaut", 125.00);
emp1.name = "Ezra Aiden";
console.log(emp1.toString());

let emp2 = new Employee(2, "Zephaniah Redd", "Clerk", 25.00);
emp2.name = "Zephaniah Redd";
console.log(emp2.toString());
