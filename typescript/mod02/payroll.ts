let payRate: number = 25;
let hoursWorked: number = 40;
let annualGross: number = (payRate * 40) * 52;
//let fileStatus: string = "Joint";



function displayDetails(name: string, payRate: number, hoursWorked: number, fileStatus: string): void {
    let grossPay: number = payRate * hoursWorked;
    let taxRate: number = .009;
    let taxes: number = grossPay * taxRate;
    let netPay: number = grossPay - taxes;
    
    console.log(`${name} worked ${hoursWorked} hours this period.`);
    console.log(`Your gross salary is ${grossPay}`);
    console.log(`Your tax withholdings this period is ${taxes}`);
    console.log(`Your net pay is ${netPay}`);

}

displayDetails("Snoopy", 15.00, 40, "single");
displayDetails("Linus", 20.00, 40, "single");
displayDetails("Charlie", 55.00, 40, "joint");


