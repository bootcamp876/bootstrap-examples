"use strict";
let scores = [93, 86, 73, 79, 83, 100, 94];
function sumNumbersInArray(scores) {
    let numScores = scores.length;
    let sumScores = 0;
    for (let i = 0; i < numScores; i++) {
        sumScores += scores[i];
    }
    return sumScores;
}
function getHighScore(scores) {
    return (scores[scores.length - 1]);
}
function getLowScore(scores) {
    return (scores[0]);
}
function displayStat(message, num) {
    console.log(`${message}: ${num}`);
}
//sumNumbersInArray(sumScores);
displayStat("High Score", getHighScore);
displayStat("Low Score", getLowScore);
