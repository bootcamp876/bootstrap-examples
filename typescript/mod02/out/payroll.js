"use strict";
let payRate = 25;
let hoursWorked = 40;
let annualGross = (payRate * 40) * 52;
//let fileStatus: string = "Joint";
function displayDetails(name, payRate, hoursWorked, fileStatus) {
    let grossPay = payRate * hoursWorked;
    let taxRate = .009;
    let taxes = grossPay * taxRate;
    let netPay = grossPay - taxes;
    console.log(`${name} worked ${hoursWorked} hours this period.`);
    console.log(`Your gross salary is ${grossPay}`);
    console.log(`Your tax withholdings this period is ${taxes}`);
    console.log(`Your net pay is ${netPay}`);
}
displayDetails("Snoopy", 15.00, 40, "single");
displayDetails("Linus", 20.00, 40, "single");
displayDetails("Charlie", 55.00, 40, "joint");
