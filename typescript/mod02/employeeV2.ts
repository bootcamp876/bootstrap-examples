let hourlyRate: number = 25;
let totalHours: number = 40;
let grossPay: number = (payRate * 40) * 52;


class EmployeeV2 {
    private id: number;

    public name: string;
    public jobTitle: string;
    public payRate: number;
    public hoursWorked: number;

    constructor(id: number, name: string, jobTitle: string, payRate: number, hoursWorked: number) {

        this.id = id;
        this.name = name;
        this.jobTitle = jobTitle;
        this.payRate = payRate;
        this.hoursWorked = hoursWorked;
    }

    toString(): string {
        return `${this.name} (${this.id}) is an ${this.jobTitle} 
            earning $${this.payRate.toFixed(2)} /hr and
            earned ${weeklyPay} this week for ${this.hoursWorked}`;
    }
}




let empV1 = new Employee(1, "Ezra Aiden", "Astronaut", 125.00, 45);
emp1.name = "Ezra Aiden";
console.log(emp1.toString());

let empV2 = new Employee(2, "Zephaniah Redd", "Clerk", 25.00, 40);
emp2.name = "Zephaniah Redd";
console.log(emp2.toString());