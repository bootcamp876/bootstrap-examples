"use strict"

let categories = [
    "Adventures",
    "Arts & Crafts",
    "Local Sports",
    "Museums",
    "Wine Tastings",
    "Other"
];

let activities = [
    {
        category: "Adventures",
        id: "A101",
        name: "Valley Hot Air Balloons",
        description: "Enjoy a lovely hot air balloon ride over the valley at sunrise. Call 800-555-1212 to reserve a date/ time after you complete your e-ticket purchase.",
        location: "121 S. Main Street",
        price: 265.00
    },
    {
        category: "Arts & Crafts",
        id: "A102",
        name: "Arts & Crafts",
        description: "Enjoy a variety of Arts & Crafts. Call 800-555-1212 to reserve a date/ time after you complete your e-ticket purchase.",
        location: "121 S. Main Street",
        price: 65.00
    },
    {
        category: "Local Sports",
        id: "A103",
        name: "Local Sports",
        description: "Call 800-555-1212 to select a sporting event and to purchase your e-ticket.",
        location: "121 S. Main Street",
        price: 75.00
    },
    {
        category: "Museums",
        id: "A104",
        name: "Museums",
        description: "Call 800-555-1212 to select a museum event and to purchase your e-ticket.",
        location: "121 S. Main Street",
        price: 55.00
    },
    {
        category: "Wine Tastings",
        id: "A105",
        name: "Wine Tastings",
        description: "Call 800-555-1212 to select a winery and purchase your e-ticket.",
        location: "121 S. Main Street",
        price: 40.00
    },
    {
        category: "Other",
        id: "A106",
        name: "Local Sports",
        description: "Call 800-555-1212 to get a list of other events and purchase your e-ticket.",
        location: "121 S. Main Street",
        price: 25.00
    }
];

window.onload = function () {
    initiateDropdownActivities();
    initiateActivityBtn();
  };

  function initiateDropdownActivities() {
    const adventuresList = document.getElementById("activitiesList");
  
    for (let i = 0; i < activities.length; i++) {
        adventuresList.appendChild(new Option(activities[i].category, activities[i].name));
    }
  }
  
  function initiateActivityBtn() {
    const btnActivities = document.getElementById("btnActivities");
    btnActivities.onclick = displayActivities;
  }

  function displayActivities() {
    const activitiesList = document.getElementById("activitiesList");
    let selectedValue = activitiesList.value;
  
    if (selectedValue == null) {
      alert("No activity was selected");
      return; // exit the event handler
    }
    else {
      const adventureArea = document.getElementById("activitiesArea");
      alert("You selected " + selectedValue);
  
      adventureArea.innerHTML = "You selected " + activities[activitiesList.selectedIndex].category + ". The price is $" + activities[activitiesList.selectedIndex].price;
    }
  }