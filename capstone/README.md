# Toni Whitfield's Capstone Project

## Description
   My Capstone project is about The T-Shirt Company who sells T-Shirts that celebrate Black History Month and any other occasion that celebrates the unique diversity of our culture.

   For my Shopping Cart, I used the code created by Charles Borst with a few modifications. 

```
git clone https://gitlab.com/bootcamp876/bootstrap-examples.git

```

## Home page
![Image](images/home.jpg "icon")
## Login page
![Image](images/login.jpg "icon")
## Registration page
![Image](images/register.jpg "icon")
## Product page
![Image](images/products.png "icon")
## Checkout page
![Image](images/cart.jpg "icon")
