"use strict";

let cities = [
    {
        name: "Detroit, MI",
        latitude: 42.3314,
        longitude: -83.0458,
    },
    {
        name: "New Orleans, LA",
        latitude: 29.9511,
        longitude: - 90.0715,
    },
    {
        name: "San Franciso, CA",
        latitude: 37.7749,
        longitude: -122.4194,
    },
    {
        name: "Las Vegas, NV",
        latitude: 36.1716,
        longitude: -115.1391
    },
    {
        name: "Chicago, IL",
        latitude: 41.8781,
        longitude: -87.6298
    },
    {
        name: "",
        latitude: 0,
        longitude: 0  
    }
]

window.onload = function () {
    initiateDropdownCities();
    initiateCitiesBtn();
};

function initiateDropdownCities() {
    const citiesList = document.getElementById("citiesList");

    for (let i = 0; i < cities.length; i++) {
        citiesList.appendChild(new Option(cities[i].name, cities[i].latitude, cities[i].longitude));
    }
}

function initiateCitiesBtn() {
    const city = document.getElementById("city");
    console.log(cities[i].name);
    city.onclick = displayCities;

}

function getData(){
    
    let url = "https://api.weather.gov/points/" + cities[i].latitude + " " + cities[i].longitude;

    fetch(url)
        .then((response) => response.json())
        .then((data) => {
            forecastUrl = data.properties.forecast;
            getForecastData();
        })
}

function getForecastData(){
    let tBody = document.getElementById("tBodyName");

    fetch(forecastUrl)
        .then((response) => response.json())
        .then((data) => {
            for (let i = 0; i < data.length; i++) {
                let row = tBody.insertRow(-1);
                
                let cell1 = tblObject.insertCell(0);
                cell1.innerHTML = data[i].name;
    
                let cell2 = tblObject.insertCell(1);
                cell2.innerHTML = data[i].latitude;

                let cell3 = tblObject.insertCell(2);
                cell3.innerHTML = data[i].longitude;

                let cell4 = tblObject.insertCell(3);
                cell4.innerHTML = data[i].forecast;
    
            }
        })
}
