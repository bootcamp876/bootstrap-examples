"use strict";

window.onload = init;

function processInput() {
    // get the input fields
    //alert("started");
    const name = document.getElementById("nameField");
}

function init() {
  const helloBtn = document.getElementById("helloBtn");
  helloBtn.onGreetUserBtnClicked = onGreetUserBtnClicked;

}

function onGreetUserBtnClicked () {     
    const nameFieldValue = document.getElementById("nameField").value;     
    const para = document.getElementById("para");      
    
    let message = `Hello ${nameFieldValue}.`;     
    para.innerHTML = message;     
    alert(message); 
}

